<?php
/**
 Template Name: Employees
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


<!-- HIDES ACF FIELDS for use with WP password protect feature -->
<?php if( !post_password_required( $post )): ?>




<section class="empWrapper">

	<div class="secHeader">
		<div class="headF">
			<div class="headD">
				<h1><?php the_field('emphead1'); ?></h1>
			</div>
			<div class="bLine"></div>
			<div class="headD">
				<h2><?php the_field('empslog1'); ?></h2>
			</div>
	</div>
</div>

	<div class="subheadCopy">
		<p><?php the_field('empann'); ?></p>
	</div>

<section class="fileDownloads">

	<?php
	if( have_rows('file_downloads') ):
	while( have_rows('file_downloads') ): the_row(); ?>
	<div class="dFile">
			<h3><?php the_sub_field('file_title'); ?></h3>
			<p><?php the_sub_field('file_desc'); ?></p>
		<a class="redButton" href="<?php the_sub_field('upload_file'); ?>"><?php the_sub_field('download_cta'); ?></a>
	</div>
	<?php endwhile; endif; ?>


</section>

</section>

<!-- ENDS HIDE ACF FIELDS with WP Password Protect -->
<?php endif; ?>

<?php the_content(); ?>

<?php get_footer(); ?>
