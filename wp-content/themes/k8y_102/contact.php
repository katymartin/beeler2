<?php
/**
 Template Name: Contact
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>



<link rel="stylesheet" href="https://cdn.rawgit.com/idiot/unslider/master/dist/css/unslider.css">
<script src="https://cdn.rawgit.com/idiot/unslider/master/dist/js/unslider-min.js"></script>


<div class="contactWrapper">
	<div class="secHeader">
	<div class="headF">
		<div class="headD">
			<h1><?php the_field('chead1'); ?></h1>
		</div>
		<div class="bLine"></div>
		<div class="headD">
			<h2><?php the_field('cslog1'); ?></h2>
		</div>
	</div>
</div>
<div class="subheadCopy">
	<p><?php the_field('contact_copy'); ?></p>
</div>
</div>

<img class="contactImgmobile" src="<?php the_field('contact_img_mobile'); ?>"/>
<img class="contactImgdesk" src="<?php the_field('contact_img_desk'); ?>"/>

<!--
	<section class="team-slide slider">


		< ?php if( have_rows('team_section') ): ?>
		< ?php while ( have_rows('team_section') ) : the_row();?>
<div class="teamMember">
		<img  src="< ?php the_sub_field('team_member'); ?>"/>
		<div class="teamBio">
			<h3>< ?php the_sub_field('team_name'); ?></h3>
			<h4>< ?php the_sub_field('team_title'); ?></h4>
			<a href="mailto:< ?php the_sub_field('team_email'); ?>" target="blank"><p><i class="fa fa-envelope" aria-hidden="true"></i><?php the_sub_field('team_email'); ?></p></a>
			<p>< ?php the_sub_field('team_bio'); ?></p>
		</div>
</div>
< ?php endwhile; endif; ?>

	</section>

<div class="greyBack"></div>
-->
<section class="contactInfo">
	<div class="contactHeader">
		<h3>Contact</h3>
	</div>
<div class="contactF">
	<div class="contactD">
		<script type="text/javascript" src="https://form.jotform.com/jsform/73094895604163"></script>
	</div>
	<div class="contactD">
		<?php include 'map.php';?>
	</div>
</div>
</section>


<?php get_footer(); ?>
