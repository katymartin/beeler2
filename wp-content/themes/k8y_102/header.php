


	<!doctype html>

	<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
	<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
	<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
	<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



		<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
	  <meta http-equiv="content-language" content="nl">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
	<script src="<?php echo get_template_directory_uri(); ?>/library/js/scripts.js"></script>
	<link href="<?php echo get_template_directory_uri(); ?>/library/css/slick.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/library/css/slick-theme.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:600,800|Source+Sans+Pro:400" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


	<link rel="canonical" href="http://beelerconstruction.com" />
	        <meta property="og:url" content="http://beelerconstruction.com"/>
	        <meta property="og:title" content="Beeler Construction" />
	        <meta property="og:description" content="Since 1959, clients have trusted us to run their projects so they can run their businesses." />
	        <meta property="og:type" content="article" />
	        <meta property="og:image" content=" /library/images/beelerWebPreview.png" />
	        <meta property="og:image:width" content="1200" />
	        <meta property="og:image:height" content="628" />



	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39015905-30"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-39015905-30');
</script>



			<title><?php wp_title(''); ?></title>

			<?php wp_head(); ?>


			<div id="preloader">
		  <div id="status">
		     <div class="spinner">
		      <div class="rect1"></div>
		      <div class="rect2"></div>
		      <div class="rect3"></div>
		      <div class="rect4"></div>
		      <div class="rect5"></div>
		    </div>
		  </div>
		</div>


<nav class="mainNav nav-down">
<a class="headLogo" href="<?php echo home_url(); ?>" target="_self"><?php include 'beelerLogo.php' ;?></a>
		<div class="mobile-nav-button">
      <div class="mobile-nav-button__line"></div>
      <div class="mobile-nav-button__line"></div>
      <div class="mobile-nav-button__line"></div>
    </div>

	  <div class="Menu">

		<div class="linkWrap">
			<div class="mainLinks">
			<div><a href="<?php echo home_url(); ?>/projects/"><h4>Projects</h4></a></div>
			<div><a href="<?php echo home_url(); ?>/capabilities/"><h4>Capabilities</h4></a></div>
			<div><a href="<?php echo home_url(); ?>/history/"><h4>History</h4></a></div>
			<div id="navCont"><a href="<?php echo home_url(); ?>/contact/"><h4>Contact</h4></a></div>
		</div>
			<div class="empLinks">
				<div><a href="<?php echo home_url(); ?>/employees/"><h4>Employees</h4></a></div>
				<div><a href="<?php echo home_url(); ?>/careers/"><h4>Careers</h4></a></div>
				<div><a href="http://www.beelerconstructionplans.com/" target="_blank"><h4>Plan Room</h4></a></div>
				<div id="socI"><a href="tel:262.252.7000" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i></a><a href="mailto:kpeterson@beelerconstruction.com"><i class="fa fa-envelope" aria-hidden="true"></i></a></div>
		</div>
	</div>
 </div>

	</nav>


		</head>


		<body id="wrapper">
