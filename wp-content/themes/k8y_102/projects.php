<?php
/**
 Template Name: Projects
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


<div class="projectMenu">
	<div class="projectLinks">
	<div><a href="#1"><h4>Healthcare</h4></a></div>
	<div><a href="#2"><h4>Retail</h4></a></div>
	<div><a href="#3"><h4>Commerical</h4></a></div>
	<div><a href="#4"><h4>Religious &amp; Educational</h4></a></div>
	<div><a href="#5"><h4>Industrial &amp; Manufacturing</h4></a></div>
</div>
</div>


<section class="projectsWrapper">

		<div class="secHeader" id="1">
			<div class="headF">
				<div class="headD">
					<h1><?php the_field('phead1'); ?></h1>
				</div>
				<div class="bLine"></div>
				<div class="headD">
					<h2><?php the_field('pslog1'); ?></h2>
				</div>
			</div>
			<div class="projectCopy">
				<p ><?php the_field('pdesc1'); ?></p>
			</div>
		</div>

		<div class="hProjF">
	 		 <?php
	 			$args = array(
	 					'post_type' => 'project',
	 					'post_status' => 'publish',
	 					'category_name' => 'healthcare',
	 					'posts_per_page' => 12	,
	 					'order' => 'DESC'
	 			);
	 			$projects_loop = new WP_Query($args);
	 			if ($projects_loop->have_posts()):
	 					while ($projects_loop->have_posts()):
	 							$projects_loop->the_post();

	 							$location = get_field('location');
	  						  $title  = get_the_title();

	 			?>
	 			 <div class="hProjD" id="projFlex">
	 			<section class="single-item slider">
	 			<?php if( have_rows('gallery-repeater') ): ?>
	 			<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
	 			<?php
	 		$images = get_sub_field('gallery-images');
	 		if( $images ):?>
	 						<?php foreach( $images as $image ): ?>
	 		<div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
	 						<?php endforeach; ?>
	 			<?php endif;
	 		endwhile;
	 		else :
	 		// no rows found
	 		endif; ?>
	 		</section>
	 		<h3><?php echo $title; ?></h3>
	 		<h4><?php echo $location; ?></h4>
	 </div>
	 				 <?php
	 					 endwhile;
	 				 wp_reset_postdata();
	 			 endif;
	 				?>
	 				</div>
	 </div>




	<div class="secHeader" id="2">
		<div class="headF">
			<div class="headD">
				<h1><?php the_field('phead2'); ?></h1>
			</div>
			<div class="bLine"></div>
			<div class="headD">
				<h2><?php the_field('pslog2'); ?></h2>
			</div>
		</div>
		<div class="projectCopy">
			<p><?php the_field('pdesc2'); ?></p>
		</div>
	</div>
	<div class="hProjF">
 		 <?php
 			$args = array(
 					'post_type' => 'project',
 					'post_status' => 'publish',
 					'category_name' => 'retail',
 					'posts_per_page' => 12	,
 					'order' => 'DESC'
 			);
 			$projects_loop = new WP_Query($args);
 			if ($projects_loop->have_posts()):
 					while ($projects_loop->have_posts()):
 							$projects_loop->the_post();

 							$location = get_field('location');
  						  $title  = get_the_title();

 			?>
 			 <div class="hProjD" id="projFlex">
 			<section class="single-item slider">
 			<?php if( have_rows('gallery-repeater') ): ?>
 			<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
 			<?php
 		$images = get_sub_field('gallery-images');
 		if( $images ):?>
 						<?php foreach( $images as $image ): ?>
 		<div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
 						<?php endforeach; ?>
 			<?php endif;
 		endwhile;
 		else :
 		// no rows found
 		endif; ?>
 		</section>
 		<h3><?php echo $title; ?></h3>
 		<h4><?php echo $location; ?></h4>
 </div>
 				 <?php
 					 endwhile;
 				 wp_reset_postdata();
 			 endif;
 				?>
 				</div>
 </div>




	<div class="secHeader" id="3">
		<div class="headF">
			<div class="headD">
				<h1><?php the_field('phead3'); ?></h1>
			</div>
			<div class="bLine"></div>
			<div class="headD">
				<h2><?php the_field('pslog3'); ?></h2>
			</div>
		</div>
		<div class="projectCopy">
			<p><?php the_field('pdesc3'); ?></p>
		</div>
	</div>

	<div class="hProjF">
 		 <?php
 			$args = array(
 					'post_type' => 'project',
 					'post_status' => 'publish',
 					'category_name' => 'comm',
 					'posts_per_page' => 12	,
 					'order' => 'DESC'
 			);
 			$projects_loop = new WP_Query($args);
 			if ($projects_loop->have_posts()):
 					while ($projects_loop->have_posts()):
 							$projects_loop->the_post();

 							$location = get_field('location');
  						  $title  = get_the_title();

 			?>
 			 <div class="hProjD" id="projFlex">
 			<section class="single-item slider">
 			<?php if( have_rows('gallery-repeater') ): ?>
 			<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
 			<?php
 		$images = get_sub_field('gallery-images');
 		if( $images ):?>
 						<?php foreach( $images as $image ): ?>
 		<div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
 						<?php endforeach; ?>
 			<?php endif;
 		endwhile;
 		else :
 		// no rows found
 		endif; ?>
 		</section>
 		<h3><?php echo $title; ?></h3>
 		<h4><?php echo $location; ?></h4>
 </div>
 				 <?php
 					 endwhile;
 				 wp_reset_postdata();
 			 endif;
 				?>
 				</div>
 </div>


 	<div class="secHeader" id="4">
 		<div class="headF">
 			<div class="headD">
 				<h1><?php the_field('phead4'); ?></h1>
 			</div>
			<div class="bLine"></div>
 			<div class="headD">
 				<h2><?php the_field('pslog4'); ?></h2>
 			</div>
 		</div>
		<div class="projectCopy">
			<p><?php the_field('pdesc4'); ?></p>
		</div>
 	</div>

	<div class="hProjF">
 		 <?php
 			$args = array(
 					'post_type' => 'project',
 					'post_status' => 'publish',
 					'category_name' => 'reled',
 					'posts_per_page' => 12	,
 					'order' => 'DESC'
 			);
 			$projects_loop = new WP_Query($args);
 			if ($projects_loop->have_posts()):
 					while ($projects_loop->have_posts()):
 							$projects_loop->the_post();

 							$location = get_field('location');
  						  $title  = get_the_title();

 			?>
 			 <div class="hProjD" id="projFlex">
 			<section class="single-item slider">
 			<?php if( have_rows('gallery-repeater') ): ?>
 			<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
 			<?php
 		$images = get_sub_field('gallery-images');
 		if( $images ):?>
 						<?php foreach( $images as $image ): ?>
 		<div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
 						<?php endforeach; ?>
 			<?php endif;
 		endwhile;
 		else :
 		// no rows found
 		endif; ?>
 		</section>
 		<h3><?php echo $title; ?></h3>
 		<h4><?php echo $location; ?></h4>
 </div>
 				 <?php
 					 endwhile;
 				 wp_reset_postdata();
 			 endif;
 				?>
 				</div>
 </div>





 <div class="secHeader" id="5">
	 <div class="headF">
		 <div class="headD">
			 <h1><?php the_field('phead5'); ?></h1>
		 </div>
		 <div class="bLine"></div>
		 <div class="headD">
			 <h2><?php the_field('pslog5'); ?></h2>
		 </div>
	 </div>
	 <div class="projectCopy">
		 <p><?php the_field('pdesc5'); ?></p>
	 </div>
 </div>


 <div class="hProjF">
		 <?php
			$args = array(
					'post_type' => 'project',
					'post_status' => 'publish',
					'category_name' => 'indman',
					'posts_per_page' => 12	,
					'order' => 'DESC'
			);
			$projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
							$projects_loop->the_post();

							$location = get_field('location');
 						  $title  = get_the_title();

			?>
			 <div class="hProjD" id="projFlex">
			<section class="single-item slider">
			<?php if( have_rows('gallery-repeater') ): ?>
			<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
			<?php
		$images = get_sub_field('gallery-images');
		if( $images ):?>
						<?php foreach( $images as $image ): ?>
		<div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
						<?php endforeach; ?>
			<?php endif;
		endwhile;
		else :
		// no rows found
		endif; ?>
		</section>
		<h3><?php echo $title; ?></h3>
		<h4><?php echo $location; ?></h4>
</div>
				 <?php
					 endwhile;
				 wp_reset_postdata();
			 endif;
				?>
				</div>
</div>


</section>


<?php get_footer(); ?>
