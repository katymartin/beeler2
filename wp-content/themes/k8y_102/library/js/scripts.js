/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

  /******************
  ******************
     PRELOADER
  ******************
  ******************/

  $(window).load(function() { // makes sure the whole site is loaded
      $("#status").fadeOut(); // will first fade out the loading animation
      $("#preloader").delay(500).fadeOut("slow"); // will fade out the white DIV that covers the website.
  })

  /*******
    MENU
  *******/
  $(document).ready(function () {
    //Menu button on click event
    $('.mobile-nav-button').on('click', function() {
      // Toggles a class on the menu button to transform the burger menu into a cross
      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");
      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");

      // Toggles a class that slides the menu into view on the screen
      $('.Menu').toggleClass('Menu--open');
      return false;
    });
  });

  /******************
  ******************
     SMOOTH SCROLL
  ******************
  ******************/


  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top -180
          }, 500);
          return false;
        }
      }
    });
  });
/******************
******************
   MENU SCROLL
******************
******************/

  // Hide Header on on scroll down
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $('.mainNav').outerHeight();

  $(window).scroll(function(event){
      didScroll = true;
  });

  setInterval(function() {
      if (didScroll) {
          hasScrolled();
          didScroll = false;
      }
  }, 250);

  function hasScrolled() {
      var st = $(this).scrollTop();

      // Make sure they scroll more than delta
      if(Math.abs(lastScrollTop - st) <= delta)
          return;

      // If they scrolled down and are past the navbar, add class .nav-up.
      // This is necessary so you never see what is "behind" the navbar.
      if (st > lastScrollTop && st > navbarHeight){
          // Scroll Down
          $('.mainNav, .Menu').removeClass('nav-down').addClass('nav-up'),
          $('.logoLine1').addClass('opacity-up'),
          $('.logoLine2').addClass('opacity-up'),
          $('.logoLine3').addClass('opacity-up'),
          $('.projectMenu').addClass('projectMenu-up'),
          $('.headLogo').addClass('logo-up');
      } else {
          // Scroll Up
          if(st + $(window).height() < $(document).height()) {
              $('.mainNav, .Menu').removeClass('nav-up').addClass('nav-down'),
              $('.logoLine1').removeClass('opacity-up'),
              $('.logoLine2').removeClass('opacity-up'),
              $('.logoLine3').removeClass('opacity-up'),
              $('.projectMenu').removeClass('projectMenu-up'),
              $('.headLogo').removeClass('logo-up');
          }
        }

      lastScrollTop = st;
  }



/******************
******************
   accordion
******************
******************/




$(document).ready(function() {

  var bodyEl = $('body'),
    accordionDT = $('.accordion').find('dt'),
    accordionDD = accordionDT.next('dd'),
    parentHeight = accordionDD.height(),
    childHeight = accordionDD.children('.content').outerHeight(true),
    newHeight = parentHeight > 0 ? 0 : childHeight,
    accordionPanel = $('.accordion-panel'),
    buttonsWrapper = accordionPanel.find('.buttons-wrapper'),
    openBtn = accordionPanel.find('.open-btn'),
    closeBtn = accordionPanel.find('.close-btn');

  bodyEl.on('click', function(argument) {
    var totalItems = $('.accordion').children('dt').length;
    var totalItemsOpen = $('.accordion').children('dt.is-open').length;

    if (totalItems == totalItemsOpen) {
      openBtn.addClass('hidden');
      closeBtn.removeClass('hidden');
      buttonsWrapper.addClass('is-open');
    } else {
      openBtn.removeClass('hidden');
      closeBtn.addClass('hidden');
      buttonsWrapper.removeClass('is-open');
    }
  });

  function openAll() {

    openBtn.on('click', function(argument) {

      accordionDD.each(function(argument) {
        var eachNewHeight = $(this).children('.content').outerHeight(true);
        $(this).css({
          height: eachNewHeight
        });
      });
      accordionDT.addClass('is-open');
    });
  }

  function closeAll() {

    closeBtn.on('click', function(argument) {
      accordionDD.css({
        height: 0
      });
      accordionDT.removeClass('is-open');
    });
  }

  function openCloseItem() {
    accordionDT.on('click', function() {

      var el = $(this),
        target = el.next('dd'),
        parentHeight = target.height(),
        childHeight = target.children('.content').outerHeight(true),
        newHeight = parentHeight > 0 ? 0 : childHeight;

      // animate to new height
      target.css({
        height: newHeight
      });

      // remove existing classes & add class to clicked target
      if (!el.hasClass('is-open')) {
        el.addClass('is-open');
      }

      // if we are on clicked target then remove the class
      else {
        el.removeClass('is-open');
      }
    });
  }

  openAll();
  closeAll();
  openCloseItem();
});


/******************
******************/

AOS.init({
        easing: 'ease-in-out-sine'
      });

      /******************
       ******************
          TEAM PAGE
       ******************
       ******************/


       'use strict';

       /* reset teamSlide scrollbar position on page load */
       $('.teamSlide-teamMember').scrollLeft(0);

       /* function to center active teamMember or scroll to leftmost or rightmost according to situation */
       var centerteamMember = function centerteamMember() {
           var teamSlideIdWidth = $('.teamSlide-teamMember').width();
           var activeWidth = $('.teamMember .active').outerWidth(true);
           var activeIndex = $('.teamMember .active').parent().index();

           var r = (teamSlideIdWidth - activeWidth) / 2;
           var s = activeWidth * activeIndex;
           var t = s - r;

           $('.teamSlide-teamMember').animate({
               scrollLeft: Math.max(0, t)
           }, 1000);
       };



       /* teamMember click: using event delegation */
       $('.teamSlide-teamMember').on('click', '.teamMember', function (e) {
           $('.teamMember a').removeClass('active');
           e.target.classList.add('active');
           var activeIndex = $('.teamMember').parent().index();
           if (activeIndex === 0) {
               $('.teamSlide-arrow-left').css('visibility', 'hidden');
               $('.teamSlide-arrow-right').css('visibility', 'visible');
           } else if (activeIndex === $('.teamMember').length - 1) {
               $('.teamSlide-arrow-right').css('visibility', 'hidden');
               $('.teamSlide-arrow-left').css('visibility', 'visible');
           } else {
               $('.teamSlide-arrow-left').css('visibility', 'visible');
               $('.teamSlide-arrow-right').css('visibility', 'visible');
           }
       });


       /* teamSlide previous and next button visible and hidden when clicking */
       $('.teamSlide-arrow-left').click(function (e) {
           e.preventDefault();
           var curPageElem = $('.teamMember .active');
           var prevPageElem = curPageElem.parent().prev().children();
           $(curPageElem).removeClass('active');
           $(prevPageElem).addClass('active');
           if (curPageElem.parent().prev().index() === 0) {
               $('.teamSlide-arrow-left').css('visibility', 'hidden');
           } else {
               $('.teamSlide-arrow-left').css('visibility', 'visible');
               $('.teamSlide-arrow-right').css('visibility', 'visible');
           }
           centerteamMember();
       });

       $('.teamSlide-arrow-right').click(function (e) {
           e.preventDefault();
           var curPageElem = $('.teamMember .active');
           var nextPageElem = curPageElem.parent().next().children();
           $(curPageElem).removeClass('active');
           $(nextPageElem).addClass('active');
           if (curPageElem.parent().next().index() === $('.teamMember').length - 1) {
               $('.teamSlide-arrow-right').css('visibility', 'hidden');
           } else {
               $('.teamSlide-arrow-right').css('visibility', 'visible');
               $('.teamSlide-arrow-left').css('visibility', 'visible');
           }
           centerteamMember();
       });



       /******************
       ******************
         SLICK settings
         ******************
         ******************/



     $(document).on('ready', function() {
       $('.single-item').slick({
     infinite: true,
     dots: true,
     slidesToShow: 1,
     slidesToScroll: 1
     });

     $('.multiple-items').slick({
       infinite: true,
       dots: true,
       slidesToShow: 4,
       responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });


   $('.team-slide').slick({
  centerMode: true,
  infinite: true,
  slidesToShow: 7,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 5
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    }
  ]
});

       });





/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function



  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();


}); /* end of as page load scripts */
