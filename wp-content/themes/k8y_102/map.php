<!DOCTYPE html>
<html>
    <head>
    <body>

       <div class="mapHeight">
       <div id="map"></div>
       <a href="https://www.google.com/maps/place/Beeler+Construction/@43.1211136,-88.1213501,17z/data=!3m1!4b1!4m5!3m4!1s0x880501a7f775a887:0x2b7f17dc2f0ba52b!8m2!3d43.1211097!4d-88.1191561" target="_blank" class="mapButton"><p>VIEW IN MAPS</p></a>






        <script type="text/javascript">
            // When the window has finished loading create our google map below

			google.maps.event.addDomListener(window, 'load', init);
        	function initMap() {

        	var uluru = {lat: 43.1211136, lng: -88.1213501};
        	var map = new google.maps.Map(document.getElementById('map'), {
          	zoom: 4,
          	center: uluru
        	});


				var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 12,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(43.1211136, -88.1213501), // New York


                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                     styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>

                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(43.1211136, -88.1213501),
                    map: map,
                    title: 'Snazzy!'
                });
            }
        </script>
        </div></div>
        <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByQezdv2K8KtJh9Rdh1NSnfVdsWXX_rsg&callback=initMap">
    </script>
     </body>
    </head>






</html>
