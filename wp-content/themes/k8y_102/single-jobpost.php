<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>


<section class="jobWrapper">
	<div class="secHeader">
				<h1><?php the_title(); ?></h1>
				<h4><?php the_field('start_date'); ?></h4>
	</div>

	<div class="jobDesc">
		<div>
		<h4><?php the_field('description_header'); ?></h4>
		<p><?php the_field('job_description'); ?></p>
		<h4 id="jMarg"><?php the_field('experience_header'); ?></h4>
		<p><?php the_field('experience_description'); ?></p>

		<?php
		if( have_rows('other_categories') ):
		while( have_rows('other_categories') ): the_row(); ?>
		<h4 id="jMarg"><?php the_sub_field('category_header'); ?></h4>
		<p><?php the_sub_field('category_description'); ?></p>
		<?php endwhile; endif; ?>

		</div>
		<div>
		<h4 id="resp"><?php the_field('resp_header'); ?></h4>
		<p><?php the_field('resp_description'); ?></p>
		<ul>
			<?php
			if( have_rows('resp_bullets') ):
			while( have_rows('resp_bullets') ): the_row(); ?>
			<li><?php the_sub_field('bullets'); ?></li>
		<?php endwhile; endif; ?>
		</ul>

	</div>

	</div>

<div class="jobForm">
<?php echo do_shortcode("[ninja_form id=2]"); ?>
</div>
</section>




<?php get_footer(); ?>
