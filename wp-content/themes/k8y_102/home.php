<?php
/**
 Template Name: Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>





<section class="homeBanner" >
	<div class="backImg" style="background: url('<?php the_field('banner_img'); ?>');"></div>
	<h1><?php the_field('home_slog'); ?></h1>
</section>

<section class="homeCall">
	<div class="callF">
		<div class="callD">
			<h3><?php the_field('home_call'); ?></h3>
		</div>
		<div class="callD">
			<p><?php the_field('home_callp'); ?></p>
		</div>
	</div>
</section>


<section class="homeWrapper">
<section class="homeProj">


	<div class="hProjF">
		<div class="hProjD" id="flex1">

			<?php
 			 $args = array(
 					 'post_type' => 'project',
 					 'post_status' => 'publish',
 					 'category_name' => 'healthcare',
 					 'posts_per_page' => 1	,
 					 'order' => 'DESC'
 			 );
 			 $projects_loop = new WP_Query($args);
 			 if ($projects_loop->have_posts()):
 					 while ($projects_loop->have_posts()):
 							 $projects_loop->the_post();

 							 $title  = get_the_title();
							 $category  = get_field('p_cat');
							 $projlink  = get_field('projlink');
							 $projcta  = get_field('projcta');

 			 ?>
			 <section class="single-item slider">
 			 <?php if( have_rows('gallery-repeater') ): ?>
 			 <?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
 			 <?php
 		 $images = get_sub_field('gallery-images');
 		 if( $images ):?>
 						 <?php foreach( $images as $image ): ?>
 		 <div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
 						 <?php endforeach; ?>
 			 <?php endif;
 		 endwhile;
 		 else :
 		 // no rows found
 		 endif; ?>
 		 </section>
 					<h3><?php echo $title; ?></h3>
 					<h4><?php echo $category; ?></h4>
 					<a class="fancyButton" href="<?php echo get_template_directory_uri(); ?><?php echo $projlink; ?>"><p><?php echo $projcta; ?></p></a>
				</div>
 					<?php
 						endwhile;
 					wp_reset_postdata();
 				endif;
 				 ?>









	 <div class="hProjD" id="flex1">
		 <?php
			 $args = array(
					 'post_type' => 'project',
					 'post_status' => 'publish',
					 'category_name' => 'retail',
					 'posts_per_page' => 1,
					 'order' => 'DESC'
			 );
			 $projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
							$projects_loop->the_post();

							$title  = get_the_title();
							$category  = get_field('p_cat');
							$projlink  = get_field('projlink');
							$projcta  = get_field('projcta');

			?>
			<section class="single-item slider">
			 <?php if( have_rows('gallery-repeater') ): ?>
			 <?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
			 <?php
		 $images = get_sub_field('gallery-images');
		 if( $images ):?>
						 <?php foreach( $images as $image ): ?>
		 <div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
						 <?php endforeach; ?>
			 <?php endif;
		 endwhile;
		 else :
		 // no rows found
		 endif; ?>
		 </section>
					<h3><?php echo $title; ?></h3>
					<h4><?php echo $category; ?></h4>
					<a class="fancyButton" href="<?php echo get_template_directory_uri(); ?><?php echo $projlink; ?>"><p><?php echo $projcta; ?></p></a>
					</div>
					<?php
						endwhile;
					wp_reset_postdata();
				endif;
				 ?>






 <div class="hProjD" id="flex2">
	 <?php
		 $args = array(
				 'post_type' => 'project',
				 'post_status' => 'publish',
				 'category_name' => 'comm',
				 'posts_per_page' => 1,
				 'order' => 'DESC'
		 );
		 $projects_loop = new WP_Query($args);
		if ($projects_loop->have_posts()):
				while ($projects_loop->have_posts()):
						$projects_loop->the_post();

						$title  = get_the_title();
						$category  = get_field('p_cat');
						$projlink  = get_field('projlink');
						$projcta  = get_field('projcta');

		?>
		<section class="single-item slider">
		 <?php if( have_rows('gallery-repeater') ): ?>
		 <?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
		 <?php
	 $images = get_sub_field('gallery-images');
	 if( $images ):?>
					 <?php foreach( $images as $image ): ?>
	 <div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
					 <?php endforeach; ?>
		 <?php endif;
	 endwhile;
	 else :
	 // no rows found
	 endif; ?>
	 </section>
				<h3><?php echo $title; ?></h3>
				<h4><?php echo $category; ?></h4>
				<a class="fancyButton" href="<?php echo get_template_directory_uri(); ?><?php echo $projlink; ?>"><p><?php echo $projcta; ?></p></a>
				</div>
				<?php
					endwhile;
				wp_reset_postdata();
			endif;
			 ?>



<div class="hProjD" id="flex2">
	<?php
		$args = array(
				'post_type' => 'project',
				'post_status' => 'publish',
				'category_name' => 'indman',
				'posts_per_page' => 1,
				'order' => 'DESC'
		);
		$projects_loop = new WP_Query($args);
	 if ($projects_loop->have_posts()):
			 while ($projects_loop->have_posts()):
					 $projects_loop->the_post();

					 $title  = get_the_title();
					 $category  = get_field('p_cat');
					 $projlink  = get_field('projlink');
					 $projcta  = get_field('projcta');

	 ?>
	 <section class="single-item slider">
		<?php if( have_rows('gallery-repeater') ): ?>
		<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
		<?php
	$images = get_sub_field('gallery-images');
	if( $images ):?>
					<?php foreach( $images as $image ): ?>
	<div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
					<?php endforeach; ?>
		<?php endif;
	endwhile;
	else :
	// no rows found
	endif; ?>
	</section>
			 <h3><?php echo $title; ?></h3>
			 <h4><?php echo $category; ?></h4>
			 <a class="fancyButton" href="<?php echo get_template_directory_uri(); ?><?php echo $projlink; ?>"><p><?php echo $projcta; ?></p></a>
			 </div>
			 <?php
				 endwhile;
			 wp_reset_postdata();
		 endif;
			?>




 <div class="hProjD" id="flex2">
	 <?php
		 $args = array(
				 'post_type' => 'project',
				 'post_status' => 'publish',
				 'category_name' => 'reled',
				 'posts_per_page' => 1,
				 'order' => 'DESC'
		 );
		 $projects_loop = new WP_Query($args);
		if ($projects_loop->have_posts()):
				while ($projects_loop->have_posts()):
						$projects_loop->the_post();

						$title  = get_the_title();
						$category  = get_field('p_cat');
						$projlink  = get_field('projlink');
						$projcta  = get_field('projcta');

		?>
		<section class="single-item slider">
		 <?php if( have_rows('gallery-repeater') ): ?>
		 <?php while ( have_rows('gallery-repeater') ) : the_row(); ?>
		 <?php
	 $images = get_sub_field('gallery-images');
	 if( $images ):?>
					 <?php foreach( $images as $image ): ?>
	 <div style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;"></div>
					 <?php endforeach; ?>
		 <?php endif;
	 endwhile;
	 else :
	 // no rows found
	 endif; ?>
	 </section>
				<h3><?php echo $title; ?></h3>
				<h4><?php echo $category; ?></h4>
				<a class="fancyButton" href="<?php echo get_template_directory_uri(); ?><?php echo $projlink; ?>"><p><?php echo $projcta; ?></p></a>
				</div>
				<?php
					endwhile;
				wp_reset_postdata();
			endif;
			 ?>



	</div>
</section>

<section class="homeTeam">
	<div class="hTeamF">
		<div class="hTeamD" id="flex1">
			<h3><?php the_field('hteam_head'); ?></h3>
			<h4><?php the_field('hteam_subhead'); ?></h4>
			<p><?php the_field('hteam_copy'); ?></p>
			<a class="fancyButton" href="<?php echo get_template_directory_uri(); ?><?php the_field('hteam_link'); ?>"><p><?php the_field('hteam_cta'); ?></p></a>
		</div>
		<div class="hTeamD" id="flex2">
			<img src="<?php the_field('hteam_img'); ?>"/>
		</div>
	</div>
</section>

</section>


<div class="greyMiddle"></div>

<?php get_footer(); ?>
