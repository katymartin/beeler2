			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">





				<script src="<?php echo get_template_directory_uri(); ?>/library/js/slick.min.js"></script>
				<script src="<?php echo get_template_directory_uri(); ?>/library/js/aos.js" rel="stylesheet"></script>

				<section class="footerWrap">
					<div class="footer">
					<div class="footerF">
						<div class="footerD">
							<h5>Contact Info</h5>
						</div>
						<div class="footerD">
							<a href="tel:1262252700"><p><i class="fa fa-phone" aria-hidden="true"></i>(p)262.252.7000</p></a>
							<a href="mailto:kpeterson@beelerconstruction.com"><p><i class="fa fa-envelope" aria-hidden="true"></i>(e)kpeterson@beelerconstruction.com</p></a>
						</div>
						<div class="footerD">
							<a href="tel:1262252700"><p><i class="fa fa-map-marker" aria-hidden="true"></i>(a) N56 W16758 Ridgewood Dr.<br/><span style="margin-left: 25px;">Menomonee Falls, WI 53051</span></p></a>
						</div>
					</div>
					<div class="copyright">
						<p>© <?php echo date("Y"); ?> Beeler Constuction</p>
					</div>
				</div>
				</section>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>



</html> <!-- end of site. what a ride! -->
