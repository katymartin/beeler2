<?php
/**
 Template Name: Careers
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>




<section class="careerWrapper">
	<div class="secHeader">
		<div class="headF">
			<div class="headD">
				<h1><?php the_field('phead1'); ?></h1>
			</div>
			<div class="bLine"></div>
			<div class="headD">
				<h2><?php the_field('pslog1'); ?></h2>
			</div>
	</div>
	</div>
	<div class="subheadCopy">
		<p><?php the_field('careers_copy'); ?></p>
	</div>

<div class="careerF">



		 <?php $loop = new WP_Query( array( 'post_type' => 'jobpost', 'posts_per_page' => 100 ) ); ?>
		 <?php while ( $loop->have_posts() ) : $loop->the_post();


		 $title  = get_the_title();
		 $startdate = get_field('start_date');
		 $careerdesc = get_field('career_desc');
		 $careercta = get_field('career_button_cta');

		 ?>
			 <div class="careerD">
				 <h3><?php echo $title; ?></h3>
				 <h4><?php echo $startdate; ?></h4>
				 <p><?php echo $careerdesc ?></p>
				 <a class="redButton" href="<?php the_permalink(); ?>"><?php echo $careercta; ?></a>
			 </div>


		 <?php endwhile; wp_reset_query(); ?>




</div>
</section>


<?php get_footer(); ?>
