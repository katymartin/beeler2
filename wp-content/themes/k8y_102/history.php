<?php
/**
 Template Name: History
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>



<section class="historyWrapper">
	<div class="secHeader">
		<div class="headF">
			<div class="headD">
				<h1><?php the_field('phead1'); ?></h1>
			</div>
			<div class="bLine"></div>
			<div class="headD">
				<h2><?php the_field('pslog1'); ?></h2>
			</div>
		</div>
	</div>
	<div class="subheadCopy">
		<p><?php the_field('history_copy'); ?></p>
	</div>
	<img class="historyFt" src="<?php the_field('history_img') ?>"/>


<div class="historyTimeline">

	<?php
	if( have_rows('history') ):
	while( have_rows('history') ): the_row(); ?>

	   <div class="timeline-block">
	      <div class="marker"></div>
	      <div class="timeline-content tFlex">
					<div class="tDiv">
	         <img src="<?php the_sub_field('history_img') ?>" />
				 </div>
				 <div class="tDiv">
					<h3><?php the_sub_field('history_title') ?></h3>
					<p><?php the_sub_field('history_event') ?></p>
				</div>
				</div>
	   </div>
<?php endwhile; endif; ?>

</div>
</section>


<section class="historyGallery">
	<section class="multiple-items slider">
		<?php if( have_rows('history_gallery') ): ?>
		<?php while ( have_rows('history_gallery') ) : the_row(); ?>
		<?php
	$images = get_sub_field('gallery-images');
	if( $images ):?>
					<?php foreach( $images as $image ): ?>
	<div><img src="<?php echo $image['sizes']['large']; ?>"/></div>
					<?php endforeach; ?>
		<?php endif;
	endwhile;
	else :
	// no rows found
	endif; ?>
	</section>
</section>


<?php get_footer(); ?>
