<?php
/**
 Template Name: Capabalities
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


<div class="page-wrap">
<div class="home">

<section class="capWrapper">
	<div class="secHeader">
		<div class="headF">
			<div class="headD">
				<h1><?php the_field('phead1'); ?></h1>
			</div>
			<div class="bLine"></div>
			<div class="headD">
				<h2><?php the_field('pslog1'); ?></h2>
			</div>
		</div>
	</div>
	<div class="subheadCopy">
		<p><?php the_field('cap_copy'); ?></p>
	</div>

<div class="accF">

<div class="accD1 accordionDiv">
<div class="accordion-panel acc1">
<div class="buttons-wrapper">
	<i class="plus-icon"></i>
	<div class="open-btn">
		Open all
	</div>
	<div class="close-btn hidden">
		Close all
	</div>
</div>
			<dl class="accordion">
					<?php
					if( have_rows('cap1') ):
					while( have_rows('cap1') ): the_row(); ?>
						<dt><h3><?php the_sub_field('capcat1'); ?></h3><i class="plus-icon"></i></dt>
						 <dd>
							<div class="content">
							<p><?php the_sub_field('capcopy1') ?></p>
							</div>
							</dd>
					<?php endwhile; endif; ?>
			</dl>
	</div>
</div>


<div class="accD2 accordionDiv">
		<div class="accordion-panel acc2">
			<dl class="accordion acc2">
				<?php
				if( have_rows('cap2') ):
				while( have_rows('cap2') ): the_row(); ?>
					<dt><h3><?php the_sub_field('capcat2'); ?></h3><i class="plus-icon"></i></dt>
					 <dd>
						<div class="content">
						<p><?php the_sub_field('capcopy2') ?></p>
						</div>
					</dd>
				<?php endwhile; endif; ?>
			</dl>
		</div>
</div>

</div>

</section>
</div>
			 </div>

<?php get_footer(); ?>
